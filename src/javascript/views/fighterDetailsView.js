import View from './view';
import { overlayElement } from '../helpers/selectors';
import { removeModal } from '../helpers/listeners';

/* Details. */
export default class FighterDetailsView extends View {
  constructor(fighter) {
    super();

    this.handleUpdate = this.handleUpdate.bind(this);
    overlayElement.addEventListener('click', removeModal, false);

    this.fighter = fighter;
    this.createDetails(fighter);
  }

  /* Create details container. */
  createDetails(fighter) {
    const nameElement = this.createElement({ tagName: 'span', className: 'name' }); // eslint-disable-line max-len
    const healthElement = this.createControl('health', fighter.health, 30, 90);
    const attackElement = this.createControl('attack', fighter.attack);
    const defenceElement = this.createControl('defence', fighter.defence);

    nameElement.innerText = fighter.name;

    /* eslint-disable max-len */
    this.element = this.createElement({ tagName: 'form', className: 'details' });
    this.element.append(nameElement, ...healthElement, ...attackElement, ...defenceElement);
    /* eslint-enable max-len */
  }

  /* Create control element. */
  createControl(name, value, min=1, max=10) {
    const labelAttributes = {
      for: name,
    };
    const inputAttributes = {
      id: name,
      type: 'range',
      value,
      min,
      max,
    };

    /* eslint-disable max-len */
    const labelElement = this.createElement({ tagName: 'label', className: 'control', attributes: labelAttributes });
    const inputElement = this.createElement({ tagName: 'input', className: 'control', attributes: inputAttributes });
    /* eslint-enable max-len */

    labelElement.innerText = `${name} [${value}]`;
    inputElement.addEventListener('change', this.handleUpdate, false);

    return [labelElement, inputElement];
  }

  /* Update attribute. */
  handleUpdate(event) {
    const targetAttribute = event.target.id;
    this.fighter[targetAttribute] = event.target.value;

    const labelElement = event.target.previousSibling;
    labelElement.innerText = `${targetAttribute} [${event.target.value}]`;
  }
}
