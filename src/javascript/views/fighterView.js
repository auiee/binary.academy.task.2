import View from './view';

/* Fighter. */
export default class FighterView extends View {
  constructor(fighter, handler) {
    super();

    this.createFighter(fighter, handler);
  }

  /* Create fighter container. */
  createFighter(fighter, handler=() => {}) {
    const { _id, source } = fighter;
    const imageElement = this.createImage(source);
    const attributes = { 'data-id': _id };

    /* eslint-disable-next-line max-len */
    this.element = this.createElement({ tagName: 'div', className: 'fighter', attributes });
    this.element.append(imageElement);
    this.element.addEventListener('click', handler, false);
  }

  /* Create fighter image. */
  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes,
    });

    return imgElement;
  }
}
