import View from './view';
import FighterView from './fighterView';
import { overlayElement } from '../helpers/selectors';
import { removeFight } from '../helpers/listeners';

/* Fight View. */
export default class FightView extends View {
  constructor(state) {
    super();

    const [first, second] = state.selected;

    this.handleKeypress = this.handleKeypress.bind(this, first, second);
    this.createFight(first, second);

    this.state = state;
    this.state.selected = [];
  }

  /* Create fight container. */
  createFight(first, second) {
    const firstFighter = new FighterView(first);
    const secondFighter = new FighterView(second);
    const indicatorElement = this.createBars();

    first.actualHealth = first.health;
    second.actualHealth = second.health;

    secondFighter.element.style.transform = 'scaleX(-1)';

    this.element = this.createElement({ tagName: 'div', className: 'fight' });
    this.element.append(firstFighter.element, secondFighter.element, indicatorElement); // eslint-disable-line max-len

    document.body.addEventListener('keypress', this.handleKeypress, false);
  }

  /* Create health bars. */
  createBars() {
    /* eslint-disable max-len */
    this.firstBar = this.createElement({ tagName: 'div', className: 'health-bar' });
    this.secondBar = this.createElement({ tagName: 'div', className: 'health-bar' });
    this.secondBar.style.transform = 'scaleX(-1)';
    /* eslint-enable max-len */

    const barContainer = this.createElement({ tagName: 'div', className: 'health-container' }); // eslint-disable-line max-len
    barContainer.append(this.firstBar, this.secondBar);

    return barContainer;
  }

  /* Handle strike keypress. */
  handleKeypress(first, second, event) {
    this._hit(first, second, this.secondBar);
    this._hit(second, first, this.firstBar);
  }

  /* End fight. */
  endFight(winner) {
    overlayElement.addEventListener('click', removeFight, false);
    overlayElement.style.display = null;
    overlayElement.state = this.state;

    const winnerElement = this.createElement({ tagName: 'span', className: 'winner' }); // eslint-disable-line max-len
    winnerElement.innerText = winner.name + ' won!';

    this.element.appendChild(winnerElement);
    document.body.removeEventListener('keypress', this.handleKeypress);
  }

  /* Simulate hit. */
  _hit(first, second, bar) {
    const damage = first.getHitPower - second.getBlockPower;
    second.actualHealth -= damage > 0 ? damage : 0;

    if (second.actualHealth < 0) return this.endFight(first);

    const percentage = second.actualHealth / second.health * 100;
    /* eslint-disable-next-line max-len */
    bar.style.backgroundImage = `linear-gradient(to right, red, red ${percentage}%, white ${percentage}%, white)`;
  }
}
