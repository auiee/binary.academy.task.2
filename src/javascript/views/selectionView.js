import View from './view';
import FightView from './fightView';
import FighterView from './fighterView';
import FighterDetailsView from './fighterDetailsView';
import { rootElement, overlayElement } from '../helpers/selectors';

/* Fighters Selection View. */
export default class SelectionView extends View {
  constructor(state) {
    super();

    this.state = state;
    this.state.selected = [];

    this.handleDetailClick = this.handleDetailClick.bind(this);
    this.handleFightClick = this.handleFightClick.bind(this);
    this.handleSelectClick = this.handleSelectClick.bind(this);

    this.createFighters(state);
  }

  /* Create fighters container. */
  createFighters(state) {
    const fighterElements = [...state.values()].map((fighter) => {
      const fighterView = new FighterView(fighter, this.handleSelectClick);
      const nameElement = this.createName(fighter, this.handleDetailClick);

      fighterView.element.appendChild(nameElement);

      return fighterView.element;
    });
    const buttonElement = this.createFight(this.handleFightClick);

    this.element = this.createElement({ tagName: 'div', className: 'selection' }); // eslint-disable-line max-len
    this.element.append(...fighterElements, buttonElement);
  }

  /* Create fighter name. */
  createName(fighter, callback) {
    const { name } = fighter;
    const nameElement = this.createElement({ tagName: 'span', className: 'name' }); // eslint-disable-line max-len

    nameElement.innerText = name;
    nameElement.addEventListener('click', callback, true);

    return nameElement;
  }

  /* Create fight button. */
  createFight(callback) {
    const fightElement = this.createElement({ tagName: 'div', className: 'fight-button' }); // eslint-disable-line max-len

    fightElement.innerText = 'fight';
    fightElement.addEventListener('click', callback, false);

    return fightElement;
  }

  /* Handle details modal. */
  handleDetailClick(event) {
    event.stopPropagation();

    const id = +event.target.parentElement.getAttribute('data-id');
    const detailsView = new FighterDetailsView(this.state.get(id));

    this.element.appendChild(detailsView.element);
    overlayElement.style.display = null;
  }

  /* Handle fight start. */
  handleFightClick(event) {
    if (this.state.selected.length != 2) return;

    const fightView = new FightView(this.state);

    rootElement.firstChild.remove();
    rootElement.appendChild(fightView.element);
  }

  /* Handle select fighter. */
  handleSelectClick(event) {
    const targetElement = (
      event.target.tagName == 'DIV' ? event.target : event.target.parentElement
    );
    const id = +targetElement.getAttribute('data-id');
    const fighter = this.state.get(id);

    /* eslint-disable-next-line max-len */
    if (this.state.selected.length < 2 && !this.state.selected.includes(fighter)) {
      this.state.selected.push(fighter);
      targetElement.classList.add('selected');
    } else {
      this.state.selected = this.state.selected.filter(x => x != fighter);
      targetElement.classList.remove('selected');
    }
  }
}
