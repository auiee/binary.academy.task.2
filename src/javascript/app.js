import SelectionView from './views/selectionView';
import { fighterService } from './services/fightersService';
import { rootElement, overlayElement } from './helpers/selectors';

/* Street Fight App. */
export default class App {
  constructor() {
    this.startApp();
  }

  /* Load fighters selection view. */
  async startApp() {
    try {
      const state = await fighterService.getFighters();
      const selectionView = new SelectionView(state);
      const selectionElement = selectionView.element;

      rootElement.appendChild(selectionElement);
    } catch (error) {
      console.warn(error);
      rootElement.innerText = 'Failed to load data';
    } finally {
      overlayElement.style.display = 'none';
    }
  }
}
