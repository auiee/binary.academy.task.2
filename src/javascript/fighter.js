/* Fighter. */
export default class Fighter {
  constructor(args) {
    for (let key in args) {
      if (Object.hasOwnProperty.call(args, key)) {
        this[key] = args[key];
      }
    }
  }

  /* Get hit power. */
  get getHitPower() {
    return this.attack * (1 + Math.random());
  }

  /* Get block power. */
  get getBlockPower() {
    return this.defence * (1 + Math.random());
  }
}
