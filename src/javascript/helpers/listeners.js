import { rootElement, overlayElement } from './selectors';
import SelectionView from '../views/selectionView';

/* Remove details modal. */
export const removeModal = function (event) {
  overlayElement.removeEventListener('click', removeModal);
  overlayElement.style.display = 'none';

  rootElement.firstChild.lastChild.remove();
};

/* Remove fight view. */
export const removeFight = async function (event) {
  const selectionView = new SelectionView(event.target.state);
  const selectionElement = selectionView.element;

  rootElement.firstChild.remove();
  rootElement.appendChild(selectionElement);

  overlayElement.removeEventListener('click', removeFight);
  overlayElement.style.display = 'none';
};
