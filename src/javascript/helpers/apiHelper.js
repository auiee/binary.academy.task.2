const API_URL = 'https://binary-studio-academy-task-3.herokuapp.com/fighters/';

/* Fetch. */
export function callApi(endpoint, method) {
  const url = API_URL + endpoint;
  const options = { method };

  return fetch(url, options)
    .then(response => (
      response.ok ? response.json() : Promise.reject(Error('Failed to load'))
    ))
    .catch((error) => {
      throw error;
    });
}
