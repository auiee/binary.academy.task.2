import Fighter from '../fighter';
import { callApi } from '../helpers/apiHelper';

/* Fighters API gateway. */
class FighterService {
  /* Get fighters list. */
  async getFighters() {
    try {
      const endpoint = '';
      const apiPayload = await callApi(endpoint, 'GET');

      const fighters = new Map();
      await Promise.all(apiPayload.map(async (fighter) => {
        const fighterDetails = await this.getFighterDetails(fighter._id);
        fighters.set(fighter._id, new Fighter(fighterDetails));
      }));

      return fighters;
    } catch (error) {
      throw error;
    }
  }

  /* Get fighter details. */
  async getFighterDetails(_id) {
    const endpoint = _id;
    const apiResult = await callApi(endpoint, 'GET');

    return apiResult;
  }
}

export const fighterService = new FighterService();
